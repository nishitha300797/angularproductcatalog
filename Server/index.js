/**
 * importing dependencies 
 */
import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'


import productRoutes from './routes/posts.js'

const app = express()
dotenv.config()

app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
// app.use(cors({
//   origin: ['http://localhost:4200'],
//   "methods": "GET,PUT,POST",
//   "preflightContinue": false,
//   "optionsSuccessStatus": 204,
//   credentials: true
// }));

app.use('/products',productRoutes) 

//database url
// const CONNECTION_URL="mongodb+srv://nmanchiravula:Nishitha1997@cluster0.j8eac.mongodb.net/Product?retryWrites=true&w=majority"

//port number
const PORT = process.env.PORT || 5000

/**
 * mongoose connection and port listening
 */
mongoose.connect(process.env.CONNECTION_URL)
.then(()=>app.listen(PORT,()=>console.log(`server running on port: ${PORT}`)))
.catch((error)=>console.log(error.message))


