import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;

  get name() {
    return this.addProductForm.get('name');
  }
  get description() {
    return this.addProductForm.get('description');
  }

  constructor(
    private productService: ProductService,
    private fb: FormBuilder
  ) {}

  //  addProductForm = new FormGroup({
  //    name :new FormControl(''),
  //    description: new FormControl(''),
  //    seller: new FormControl(''),
  //    quantity : new FormControl(),
  //    price: new FormControl()

  //  })

  //  productModel = new Product("microwave","fccgvv","lg",5000,1)
  //  submitted = false
  errorMsg = '';

  ngOnInit() {
    this.addProductForm = this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z]).{5,30}$'),
        ],
      ],
      description: [
        '',
        [
          Validators.required,
          Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z]).{5,30}$'),
        ],
      ],
      seller: ['', [Validators.required]],
      quantity: [''],
      price: [''],
    });
  }

  onSubmit() {
    console.log(this.addProductForm.value);
    this.productService.postProduct(this.addProductForm.value).subscribe({
      next: (data) => {
        console.log('Product successfully added to the database', data);
      },
      error: (error) => {
        this.errorMsg = error.statusText;
        console.log('there is an error !', this.errorMsg);
      },
    });

    this.addProductForm.reset();
  }

  //   onSubmit(){
  //   this.submitted = true

  //   this.productService.postProduct(this.productModel).subscribe({
  //     next: data => {
  //         console.log('Product successfully added to the database' ,data)
  //     },
  //     error: error => {
  //         this.errorMsg = error.statusText
  //         console.log('There was an error!',this.errorMsg)
  //     }
  // })

  //   }
}
