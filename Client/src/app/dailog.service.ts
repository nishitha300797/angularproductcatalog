import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeletepopupComponent } from './confirmdailog/deletepopup.component';

@Injectable({
  providedIn: 'root'
})
export class DailogService {

  constructor(public dialog:MatDialog ) {}

  openConfirmDailog(){
    return this.dialog.open(DeletepopupComponent,{
      width: '400px',
      disableClose:true
    })
  }
}
