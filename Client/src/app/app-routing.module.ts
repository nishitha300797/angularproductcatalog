import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { HomeComponent } from './home/home.component';
import { UpdateComponent } from './update/update.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
  {path:"",component:HomeComponent},
  {path :"products", component:ProductComponent},
  {path:"addproduct", component:AddProductComponent},
  {path:"updateproduct/:id" ,component:UpdateComponent},
  {path:"**" ,component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [ProductComponent,AddProductComponent,HomeComponent,UpdateComponent,PagenotfoundComponent]
