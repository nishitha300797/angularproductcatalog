import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef ,MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-deletepopup',
  templateUrl: './deletepopup.component.html',
  styleUrls: ['./deletepopup.component.css']
})
export class DeletepopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data,public dailogRef: MatDialogRef<DeletepopupComponent>) { }

  ngOnInit(): void {
  }

  closeDialog(){
    this.dailogRef.close(false)
  }
}
