import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProduct } from './assets/product';
import { Product } from './product';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  public headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  

  public getProducts(): Observable<IProduct> {
    return this.http
      .get<IProduct[]>('http://localhost:5000/products', {
        headers: this.headers,
      })
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  public postProduct(product: Product) {
    return this.http
      .post<any>('http://localhost:5000/products', product)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(() => error);
  }

  public deleteProduct(productId: any) {
    console.log('product id', productId);
    const url = 'http://localhost:5000/products/' + productId;
    // console.log(url);

    return this.http.delete<any>(url).pipe((res) => {
     
      return res;
    });
  }

  public getProductById(id: any) {
    const url = 'http://localhost:5000/products/' + id;

    return this.http.get(url).pipe((res) => {
      return res;
    });
  }

  public updateProduct(id: any, data: any) {
    console.log('data', data);
    const url = 'http://localhost:5000/products/' + id;

    return this.http.patch(url, data).pipe((res) => {
      return res;
    });
  }
}
