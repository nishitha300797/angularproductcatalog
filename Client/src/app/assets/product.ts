export interface IProduct{
  name:string,
  description:string,
  seller:string,
  price:number,
  quantity:number
}