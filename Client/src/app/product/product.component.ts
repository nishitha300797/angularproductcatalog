import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

import {
  MatDialog,
} from '@angular/material/dialog';

import { DailogService } from '../dailog.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  products: any;
  errorMsg :''
  constructor(
    private productService: ProductService,
    public dialog: MatDialog,
    public dialogService:DailogService 
  ) {}



  ngOnInit(): void {
    this.GetProducts()
   }

   
  // GetProducts(){
  //   this.productService.getProducts().subscribe((data) => {

  //     this.products = data;
  //     console.log('products', this.products);
  //   });
  // }

  GetProducts(){
    this.productService.getProducts().subscribe({
      next:(data)=>{
        this.products = data;
        console.log('products',this.products)
      },
      error:(error)=>{
       this.errorMsg = error.statusText;
       console.log("there is an error!", this.errorMsg)
      }
    })
  }

  deleteProduct(productId:any,i:any) {
   
    this.dialogService.openConfirmDailog().afterClosed().subscribe(res=>{
      if(res){
        this.products.splice(i,1)
        this.productService.deleteProduct(productId).subscribe(res=>{
          console.log(res)
        })
      }
    })
   
  }
  
 
}
