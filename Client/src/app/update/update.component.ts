import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { ProductService } from '../product.service';
import { Product } from '../product';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { UpdatesuccessComponent } from '../updatesuccess/updatesuccess.component';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  updateProduct = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    seller: new FormControl(''),
    quantity: new FormControl(''),
    price: new FormControl(''),
  });

  constructor(
    private productService: ProductService,
    private router: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UpdatesuccessComponent>
  ) {}

  ngOnInit(): void {
    this.productService
      .getProductById(this.router.snapshot.params['id'])
      .subscribe((result) => {
        //   console.log(result);

        this.updateProduct = new FormGroup({
          name: new FormControl(result['name']),
          description: new FormControl(result['description']),
          seller: new FormControl(result['seller']),
          price: new FormControl(result['price']),
          quantity: new FormControl(result['quantity']),
        });
      });
  }

  openDialog(): void {
    const dialogRef1 = this.dialog.open(UpdatesuccessComponent, {
      width: '400px',
    });

    dialogRef1.afterClosed().subscribe((result: any) => {
      result = console.log('The dialog was closed');
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onUpdate() {
    this.openDialog();
    console.log(this.updateProduct.value);
    this.productService
      .updateProduct(
        this.router.snapshot.params['id'],
        this.updateProduct.value
      )
      .subscribe((result) => {
        console.log(result, 'data updated successfully');
      });
    this.updateProduct.reset();
  }
}
